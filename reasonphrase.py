#!/usr/bin/python

import requests

def want_ok(URI):
    r = requests.get(URI)
    print "Status code: " + str(r.status_code)
    print "Reason: " + r.reason
    print "OK? " + str(r.ok)

def main():
    # base = str(input("What URL? "))
    # port = input("What port number? ")
    # url = "http://" + base + ":" + port
    want_ok("http://localhost:8000")

if __name__ == "__main__":
    main()
