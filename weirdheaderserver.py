#!/usr/bin/python3

import http.server

class OddHeaderHTTPRequestHandler(http.server.SimpleHTTPRequestHandler):
    responses = dict(http.server.SimpleHTTPRequestHandler.responses)
    responses[200] = ('Oll Korrect', 'Oh Kay')
    responses[404] = ('I Know Nothing', 'Nothing here by that name')

    def end_headers(self):
        self.send_header("Glub", "Sumanariffic")
        super(OddHeaderHTTPRequestHandler, self).end_headers()

def main():
    http.server.test(HandlerClass=OddHeaderHTTPRequestHandler,
 ServerClass=http.server.HTTPServer, protocol="HTTP/1.0", port=8000, bind="")

if __name__ == "__main__":
    main()
