#!/usr/bin/python

import BaseHTTPServer
import os


PORT = 8000

class APIHTTPRequestHandler(BaseHTTPServer.BaseHTTPRequestHandler):

    rbufsize = 0

    def do_GET(self):
        def respond_with_file(name, content_type):
            self.send_response(200)
            with open(name) as f:
                self.response_body = f.read()
                self.send_header("Content-Length", str(len(self.response_body)))
                self.send_header("Content-Type", content_type)
                self.end_headers()
                self.wfile.write(self.response_body)
        if self.path == '/':
            respond_with_file("index.html", "text/html")
        elif self.path == '/horrorworld.jpg':
            respond_with_file("horrorworld.jpg", "image/jpeg")
        elif self.path == '/FileToDelete.txt':
            respond_with_file("FileToDelete.txt", "text/plain")
        else:
            self.send_response(404)
            self.end_headers()

    def do_DELETE(self):
        def delete_file(name):
            self.send_response(204)
            os.remove(name)
            self.end_headers()
        if self.path == '/':
            self.send_response(403)
            self.end_headers()
        elif self.path == '/FileToDelete.txt':
            delete_file("FileToDelete.txt")
        else:
            self.send_response(404)
            self.end_headers()


def main(server_class=BaseHTTPServer.HTTPServer,
        handler_class=APIHTTPRequestHandler):
    server_address = ('', PORT)
    httpd = server_class(server_address, handler_class)
    print "serving at port", PORT
    httpd.serve_forever()

if __name__ == '__main__':
    main()

